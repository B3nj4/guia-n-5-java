
public class Libros extends Letras {

	private int codigo;
	private String titulo;
	private int año;
	Boolean prestado;
	private int deuda;
	
	//Constructor
	Libros(){
		this.codigo = codigo();
		this.titulo = titulo();
		this.año = añopublicacion();
		this.prestado = false;
		this.deuda = 0;
	}
	
	public Boolean getPrestado() {
		return prestado;
	}

	public void setPrestado(Boolean prestado) {
		this.prestado = prestado;
	}
	
	public int getcodigo() {
		return codigo;
	}
	
	public String getTitulo() {
		return titulo;
	}
	
	public int getAño() {
		return año;
	}
	
	public int getDeuda() {
		return deuda;
	}
	
	public void aumenta_deuda_diaria() {
		this.deuda += 1290;
	}
	
	public void prestar() {
		this.prestado = true;
	}

	public void devolver() {
		this.prestado = false;						
	}			
	
	
	public void muestra_detalle_libro(){
		System.out.print("   " + getTitulo() + " - " + getAño() + " - " + "ISBN:" + getcodigo() + " - " + "Deuda: $ " + getDeuda());
		if (getPrestado() == false) {
			System.out.print(" - Disponible");
		}
		else if (getPrestado() == true) {
			System.out.print(" - Prestado");
		}
		
	}
}
