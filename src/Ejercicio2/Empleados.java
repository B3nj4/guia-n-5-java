package Ejercicio2;

import java.util.Random;

public class Empleados extends Persona{
	
	private int añoin;
	private int numanexo;
		
	public int añoincorporacion() {
		Random numaleatorio = new Random();
		this.añoin = numaleatorio.nextInt((2021-1990+1)+1990);	
		return this.añoin;
	}
	
	public int numanexo() {
		Random numaleatorio = new Random();
		this.numanexo = numaleatorio.nextInt(1000);	
		return this.numanexo;
	}
	
}