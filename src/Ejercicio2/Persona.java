package Ejercicio2;

import java.util.Random;

class Persona {
	
	private String nombre;
	private String apellido;
	private int id;
		
	public String nombre() {
		//Scanner sc = new Scanner(System.in);
		//System.out.print("Introduzca nombre de la persona: ");
		//nombre = sc.nextLine();
		return this.nombre;
	}
	
	public String apellido() {
		//Scanner sc = new Scanner(System.in);
		//System.out.print("Introduzca apellido de la persona: ");
		//apellido = sc.nextLine();
		return this.apellido;
	}
	
	public int id() {
		Random numaleatorio = new Random();
		this.id = numaleatorio.nextInt(100000);	
		return this.id;
	}	
}
