package Ejercicio2;

import java.util.Scanner;

public class Controlador {
	
	private String name;
	private String surname;
	private Estudiantes e;
	private Profesores p;
	private Administrativos a;
	
	Controlador(){
		
		this.e = new Estudiantes();
		this.p = new Profesores();
		this.a = new Administrativos();
		
		for (int i=0; i<1;i++) {

			muestra_por_nombre(busca_nombre(), busca_apellido());
		}
	}
	
	
	
	public String busca_nombre() {
		Scanner sc = new Scanner(System.in);
		System.out.print("Introduzca nombre: ");
		name = sc.nextLine();
		return name;
	}
	
	
	public String busca_apellido() {
		Scanner sc = new Scanner(System.in);
		System.out.print("Introduzca apellido: ");
		surname = sc.nextLine();
		return surname;
	}
	
	public void muestra_por_nombre(String name, String surname) {
		if (e.getNombre().equals(name) && e.getApellido().equals(surname)) {
			e.impresion_Estudiante(); 
		}
		
		else if (p.getNombre().equals(name) && p.getApellido().equals(surname)) {
			p.impresion_Profesor(); 
		}
		
		else if (a.getNombre().equals(name) && a.getApellido().equals(surname)) {
			a.impresion_Administrativo(); 
		}
		
		else {
			System.out.println("No coincide con ningun nombre");
		}
	
	}
}
