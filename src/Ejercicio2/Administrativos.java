package Ejercicio2;


import java.util.Random;

public class Administrativos extends Empleados{
	
	private String nombre;
	private String apellido;
	private int id;
	private String seccion;
	
	//Constructor
	Administrativos(){
		this.nombre = "Larry";
		this.apellido = "Capizza";
		this.id = id();
		this.seccion = randSeccion();
	}
	
	public String randSeccion(){
		String seccion[] = new String[]{"biblioteca", "secretarı́a", "decanato", };
		this.seccion = seccion[new Random().nextInt(seccion.length)];
		return this.seccion;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	public String getApellido() {
		return this.apellido;
	}
	
	public void impresion_Administrativo() {
		System.out.println("Administrativo: ");
		System.out.println(this.nombre + " - " + this.apellido + " - " + this.id + " - " + this.seccion);
	}
}
