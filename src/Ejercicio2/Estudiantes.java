package Ejercicio2;

import java.util.Random;

public class Estudiantes extends Persona{
	
	private String nombre;
	private String apellido;
	private int id;
	private String curso;
	
	//constructor
	Estudiantes(){
		this.nombre = "Lola";
		this.apellido = "Mento";
		this.id = id();
		this.curso = randCurso();
	}
	
	public String randCurso(){
		String curso[] = new String[]{"1° año", "2° Año", "3° año", "4° año"};
		this.curso = curso[new Random().nextInt(curso.length)];
		return this.curso;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	public String getApellido() {
		return this.apellido;
	}
	
	public void impresion_Estudiante() {
		System.out.println("Estudiante: ");
		System.out.println(this.nombre + " - " + this.apellido + " - " + this.id + " - " + this.curso);
	}
	
}
