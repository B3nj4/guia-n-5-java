package Ejercicio2;

import java.util.Random;

public class Profesores extends Empleados{
	
	private String nombre;
	private String apellido;
	private int id;
	private String departamento;
	
	//constructor
	Profesores(){
		this.nombre = "Elgar";
		this.apellido = "Gajo";
		this.id = id();
		this.departamento = randDepartamento();
	}
	
	public String randDepartamento(){
		String departamento[] = new String[]{"Bioinformatica", "Computación", "Mécanica", "VideoJuegos"};
		this.departamento = departamento[new Random().nextInt(departamento.length)];
		return this.departamento;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	public String getApellido() {
		return this.apellido;
	}
	
	public void impresion_Profesor() {
		System.out.println("Profesor: ");
		System.out.println(this.nombre + " - " + this.apellido + " - " + this.id + " - " + this.departamento);
	}
	
}
