
import java.util.Random;

public class Revistas extends Letras {
	
	private int codigo;
	private String titulo;
	private int año;
	private String clase;
	private int emision;
	private String categoria;
	private Boolean caducidad;
	private Boolean prestado; //prestado false = esta disponible, presado true = no esta disponible (esta en posesion)
	
	Revistas(){
		this.codigo = codigo();
		this.titulo = titulo();
		this.año = añopublicacion();
		this.emision = randNumeroEmision();
		this.categoria = randCategoria();
		this.caducidad = false;
		this.prestado = false;
	}

	public int getNumeroEmision() {
		return emision;
	}
	
	public int getcodigo() {
		return codigo;
	}
	
	public String getTitulo() {
		return titulo;
	}
	
	public int getAño() {
		return año;
	}
	
	public String getCategoria() {
		return categoria;
	}
	
	public Boolean getCaducidad() {
		return caducidad;
	} 
	
	public Boolean getPrestado() {
		return prestado;
	} 
	
	public void setPrestado(Boolean prestado) {
		this.prestado = prestado;
	}
	
	public void prestar() {
		this.prestado = true;
	}

	public void devolver() {
		this.prestado = false;						
	}
	
	public void caducar() {
		this.caducidad = true;
	}
	
	
	
	public String randCategoria() {
		String categoria[] = new String[]{"comic", "cientifica", "deportiva", "gastronomica"};
		this.categoria = categoria[new Random().nextInt(categoria.length)];
		return this.categoria;
	}
	
	public int randNumeroEmision() {
		Random numaleatorio = new Random();
		this.emision = numaleatorio.nextInt(100);
		return this.emision;
	}
	
	public void muestra_detalle_revista(){
		System.out.print("\n   " + getTitulo() + " - " + getAño() + " - " + "ISBN:" + getcodigo() + " - " + getNumeroEmision() + " - " + getCategoria());
		if (getPrestado() == false) {
			System.out.print(" - Disponible");
		}
		else if (getPrestado() == true) {
			System.out.print(" - Prestado");
		}
		
		if (getCaducidad() == false) {
			System.out.print(" - No esta caducada");
		}
		else if (getCaducidad() == true) {
			System.out.print(" - Caducada");
		}
		
	}
}
