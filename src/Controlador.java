import java.util.Scanner;

/*
interface control {
	public void prestar();
	public void devolver();
	//public void prestado();
}

*/

public class Controlador {
	
	private Libros l;
	private Revistas r;
	private int teclado;
	
	Controlador(){
		
		System.out.println("< Ingresar datos del libro >");
		this.l = new Libros();
		System.out.println("< Ingresar datos de la revista >");
		this.r = new Revistas();
				
		for (int dia = 1; dia <= 10;dia ++) {
			System.out.println("\n\n< En el dia " + dia + " sucedio lo siguiente >");
			System.out.println("-> Informacion del libro por el momento: " );
			l.muestra_detalle_libro();
			r.muestra_detalle_revista();
			
			System.out.println("\n-> Tipo de literatura a la cual visualizar(Libro(0) o Revista(1)): ");
			Scanner sc = new Scanner(System.in);
			teclado = sc.nextInt();
			
			//libro
			if (teclado == 0) {
				System.out.println("-> Desea Prestar(10) o Devolver(20)");
				Scanner sc1 = new Scanner(System.in);
				teclado = sc1.nextInt();
				//prestar
				if (teclado == 10) {
					if (l.getPrestado() == false) {
						l.prestar();
						System.out.println("-El libro ha sido prestado ");
					}
					else {
						System.out.println("-Libro no apto para prestar");
					}
				}
				
				//devolver
				else if (teclado == 20) {
					if (l.getPrestado() == true && dia <= 5) {
						l.devolver();
						System.out.println("-El libro ha sido devuelto ");
					}
					else {
						System.out.println("-Libro no apto para devolver");
					}
				}
				
				//deuda
				if (l.getPrestado() == true && dia > 5) {
					l.aumenta_deuda_diaria();
					System.out.println("-Aumenta deuda por atraso");
				}
			}
			
			
			//revista
			else if (teclado == 1) {
				System.out.println("-> Desea Prestar(10) o Devolver(20)");
				Scanner sc1 = new Scanner(System.in);
				teclado = sc1.nextInt();
				//prestar
				if (teclado == 10) {
					if (r.getPrestado() == false && r.getCaducidad() == false) {
						 if (dia <= 1 && r.getCategoria() == "comic" ) {
							 r.prestar();
							 System.out.println("-La revista ha sido prestada ");
						 }
						 
						 else if(dia <= 2 && r.getCategoria() == "cientifica" ) {
							 r.prestar();
							 System.out.println("-La revista ha sido prestada ");
						 }
						 
						 else if(dia <= 3 && r.getCategoria() == "deportiva" || dia <= 3 && r.getCategoria() == "gastronomica") {
							 r.prestar();
							 System.out.println("-La revista ha sido prestada ");
						 }
						 else {
							 System.out.println("-No es posible prestar la revista");
						 }
					}
					else {
						System.out.println("-Revista no apta para prestar");
					}
				}
				
				//devolver
				else if (teclado == 20) {
					if (r.getPrestado() == true && r.getCaducidad() == false) {
						 if (dia <= 1 && r.getCategoria() == "comic" ) {
							 r.devolver();
							 System.out.println("-La revista ha sido devuelta ");
						 }
						 
						 else if(dia <= 2 && r.getCategoria() == "cientifica" ) {
							 r.devolver();
							 System.out.println("-La revista ha sido devuelta ");
						 }
						 
						 else if(dia <= 3 && r.getCategoria() == "deportiva" || dia <= 3 && r.getCategoria() == "gastronomica") {
							 r.devolver();
							 System.out.println("-La revista ha sido devuelta ");
						 }
						 else {
							 System.out.println("-No es posible devolver la revista");
						 }
					}
					else {
						System.out.println("-Revista no apta para devolver");
					}
				}
				
				//caducidad
				if (r.getCaducidad() == false && r.getPrestado() == true) {
					if (dia > 1 && r.getCategoria() == "comic") {
						r.caducar();
						System.out.println("-Revista ha caducado");
					 }
					 
					 else if (dia > 2 && r.getCategoria() == "cientifica" ) {
						 r.caducar();
						 System.out.println("-Revista ha caducado");
					 }
					 
					 else if (dia > 3 && r.getCategoria() == "deportiva" || dia > 3 && r.getCategoria() == "gastronomica") {
						 r.caducar();
						 System.out.println("-Revista ha caducado");
					 }						
				}
			}
			
				
					
		}			
	}			
}
		
	

